# djMySite #

Simple Blog Application with Comment system

### Features ###

* Posts with tags (django-taggit)
* Share post via email
* Display Similar and Most Commented Posts
* Sitemap
* RSS Feed
* Search Engine with Solr and Haystack

### Requirements ###

* Python 3.3+
* requirements.txt
* [Solr 4.10](http://archive.apache.org/dist/lucene/solr/)