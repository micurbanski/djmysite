# django imports
from django import forms
# my imports
from .models import Comment


class EmailPostForm(forms.Form):
    """
    Class for sharing posts through email with users
    """
    name = forms.CharField(max_length=30)
    email = forms.EmailField()
    to = forms.EmailField()
    comments = forms.CharField(required=False, widget=forms.Textarea)


class CommentForm(forms.ModelForm):
    """
    ModelForm for comments under the post
    """
    class Meta:
        model = Comment
        fields = ('name', 'email', 'body')


class SearchForm(forms.Form):
    """
    Class form for search queries
    """
    query = forms.CharField()
