from django.contrib.syndication.views import Feed
from django.template.defaultfilters import truncatewords

from .models import Post


class LatestPostsFeed(Feed):
    """
    class for feeding last 5 posts through RSS
    """
    title = 'Custom Blog'
    link = '/blog/'
    description = 'Last 5 posts'

    def items(self):
        return Post.published.all()[:5]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return truncatewords(item.body, 30)
