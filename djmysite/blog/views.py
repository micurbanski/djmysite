# djanog imports
from django.db.models import Count
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView
# 3rd party imports
from taggit.views import Tag
from haystack.query import SearchQuerySet
# my imports
from .forms import EmailPostForm, CommentForm, SearchForm
from .models import Post

# Create your views here.


def post_share(request, post_id):
    # get post by id:
    post = get_object_or_404(Post, id=post_id, status='published')
    sent = False

    if request.method == 'POST':
        # form submitted
        form = EmailPostForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            post_url = request.build_absolute_uri(post.get_absolute_url())

            # "$User ($user@user) recommends you reading "$title"
            subject = '{} ({}) recommends you reading "{}"'.format(
                    cleaned_data['name'],
                    cleaned_data['email'],
                    post.title)
            # "Read "$title" at $url \n\n $User's comments: $comment
            message = 'Read "{}" at {}\n\n{}\'s comments:\n {}'.format(
                    post.title,
                    post_url,
                    cleaned_data['name'],
                    cleaned_data['comments'])
            send_mail(subject, message, 'admin@blog.com', [cleaned_data['to']])
            sent = True
    else:
        form = EmailPostForm()
    return render(request, 'blog/post/share.html', {'post': post,
                                                    'form': form,
                                                    'sent': sent})


def post_list(request, tag_slug=None):
    object_list = Post.published.all()
    tag = None

    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        object_list = object_list.filter(tags__in=[tag])

    paginator = Paginator(object_list, 3)
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    return render(request, 'blog/post/list.html', {'page': page,
                                                   'posts': posts,
                                                   'tags': tag})


class PostListView(ListView):
    queryset = Post.published.all()
    context_object_name = 'posts'
    paginate_by = 3
    template_name = 'blog/post/list.html'


def post_detail(request, year, month, day, post):
    post = get_object_or_404(Post, slug=post,
                             status='published',
                             publish__year=year,
                             publish__month=month,
                             publish__day=day)
    # list of active comments goes here:
    comments = post.comments.filter(active=True)

    if request.method == 'POST':
        # comment posted
        comment_form = CommentForm(data=request.POST)

        if comment_form.is_valid():
            # Assign post to comment first!!
            new_comment = comment_form.save(commit=False)
            new_comment.post = post
            # Now Save
            new_comment.save()
    else:
        comment_form = CommentForm()

    # list of similar posts based on tag count
    post_tags_id = post.tags.values_list('id', flat=True)
    similar_posts = Post.published.filter(tags__in=post_tags_id).exclude(
            id=post.id)
    similar_posts = similar_posts.annotate(same_tags=Count('tags')).order_by(
            '-same_tags', '-publish')[:4]

    return render(request,
                  'blog/post/detail.html',
                  {'post': post,
                   'comments': comments,
                   'comment_form': comment_form,
                   'similar_posts': similar_posts})


def post_search(request):
    form = SearchForm()
    context = {'form': form}

    if 'query' in request.GET:
        form = SearchForm(request.GET)

        if form.is_valid():
            clean_data = form.cleaned_data
            results = SearchQuerySet().models(Post).filter(
                                        content=clean_data['query']).load_all()
            total_results = results.count()
            context = {
                'form': form,
                'clean_data': clean_data,
                'results': results,
                'total_results': total_results
            }

    return render(request, 'blog/post/search.html', context)
